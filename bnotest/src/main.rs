#![no_main]
#![no_std]

use panic_semihosting as _;
use rtfm::app;

use cortex_m::asm;

// use heapless::mpmc::Q8;

use stm32f1xx_hal::{
    prelude::*,
    pac,
    delay::Delay,
    gpio::{
        gpiob,
        OpenDrain,
        Alternate,
    },
    timer::{Timer, CountDownTimer},
    i2c::{
        Mode,
        BlockingI2c,
    }
};
use bno055::Bno055;

type BnoPins = (gpiob::PB6<Alternate<OpenDrain>>, gpiob::PB7<Alternate<OpenDrain>>);

#[app(device = stm32f1xx_hal::pac, peripherals = true)]
const APP: () = {
    struct Resources {
        timer: CountDownTimer<pac::TIM2>,
        bno: Bno055<BlockingI2c<pac::I2C1, BnoPins>>
    }

    #[init]
    fn init(ctx: init::Context) -> init::LateResources {
        // Alias peripherals
        let cp: cortex_m::Peripherals = ctx.core;
        let dp: pac::Peripherals = ctx.device;

        // Set up core registers
        let mut flash = dp.FLASH.constrain();
        let mut rcc = dp.RCC.constrain();
        let mut afio = dp.AFIO.constrain(&mut rcc.apb2);

        let clocks = rcc.cfgr
            .use_hse(8.mhz())
            .sysclk(72.mhz())
            .pclk1(36.mhz())
            .pclk2(36.mhz())
            .adcclk(7.mhz())
            .freeze(&mut flash.acr);

        assert!(clocks.usbclk_valid());

        // Set up GPIO registers
        let mut gpiob = dp.GPIOB.split(&mut rcc.apb2);

        // Configure delay
        let mut delay = Delay::new(cp.SYST, clocks);

        // Set up the timer
        let timer = Timer::tim2(dp.TIM2, &clocks, &mut rcc.apb1)
            .start_count_down(2.hz());


        let i2c_pins = (
            gpiob.pb6.into_alternate_open_drain(&mut gpiob.crl),
            gpiob.pb7.into_alternate_open_drain(&mut gpiob.crl),
        );
        let i2c = BlockingI2c::i2c1(
            dp.I2C1,
            i2c_pins,
            &mut afio.mapr,
            Mode::Standard{frequency: 100.khz().into()},
            clocks,
            &mut rcc.apb1,
            1_000,
            1,
            1_000,
            1_000,
        );
        let mut bno = Bno055::new(i2c)
            .with_alternative_address();
        bno.init(&mut delay).unwrap();
        bno.set_mode(bno055::BNO055OperationMode::NDOF, &mut delay)
            .expect("Failed to set mode");

        init::LateResources {
            timer,
            bno,
        }
    }

    #[idle(resources = [bno])]
    fn idle(ctx: idle::Context) -> ! {
        let mut _r = ctx.resources;
        loop {
            // i = (i + 1)%255;
            asm::wfi();
        }
    }

};

#![no_std]
#![no_main]


extern crate panic_semihosting;

use cortex_m::asm;
use cortex_m_semihosting::hprintln;

use rtfm::app;


use stm32f1xx_hal::{
    prelude::*,
    pac,
    timer::{self, Timer},
    pwm::{Pwm, C1, C2, C3, C4},
    serial::{self},
    gpio::{self}
};
use heapless::spsc::{Consumer, Producer, Queue};
use typenum::consts::{U8, U32};
use nb::block;

use rc_protocols_rs::{
    sbus::{self, SbusFrame, SbusDecoder}
};

use pac::TIM4;

type SbusResult = sbus::RecoverableResult<SbusFrame, serial::Error>;

// Allocating this here because it needs to have a static lifetime
static mut BYTE_QUEUE: Option<Queue<Result<u8, serial::Error>, U32>> = None;
static mut SBUS_QUEUE: Option<Queue<SbusResult, U8>> = None;

#[app(device = stm32f1xx_hal::pac)]
const APP: () = {
    static mut PWM_CHANNELS: (
        Pwm<TIM4, C1>,
        Pwm<TIM4, C2>,
        Pwm<TIM4, C3>,
        Pwm<TIM4, C4>
    ) = ();
    static mut BYTE_TX: Producer<'static, Result<u8, serial::Error>, U32> = ();
    static mut SBUS_RX: Consumer<'static, SbusResult, U8> = ();
    static mut SBUS_DECODER: SbusDecoder<'static, serial::Error> = ();
    static mut SBUS: serial::Rx<pac::USART1> = ();
    static mut LED: gpio::gpioc::PC13<gpio::Output<gpio::PushPull>> = ();

    #[init(resources = [])]
    fn init() -> init::LateResources {
        let mut rcc = device.RCC.constrain();
        let mut flash = device.FLASH.constrain();
        let clocks = rcc.cfgr.sysclk(36_000_000.hz()).freeze(&mut flash.acr);

        // Init timer
        let mut tim2 = Timer::tim2(device.TIM2, 1000.hz(), clocks, &mut rcc.apb1);
        tim2.listen(timer::Event::Update);

        let mut afio = device.AFIO.constrain(&mut rcc.apb2);

        // Set up LED output
        let mut gpioc = device.GPIOC.split(&mut rcc.apb2);
        let led = gpioc.pc13.into_push_pull_output(&mut gpioc.crh);

        // Set up ppm output
        let mut gpiob = device.GPIOB.split(&mut rcc.apb2);

        let pins = (
            gpiob.pb6.into_alternate_push_pull(&mut gpiob.crl),
            gpiob.pb7.into_alternate_push_pull(&mut gpiob.crl),
            gpiob.pb8.into_alternate_push_pull(&mut gpiob.crh),
            gpiob.pb9.into_alternate_push_pull(&mut gpiob.crh),
        );

        let pwm_channels = device.TIM4.pwm(
            pins,
            &mut afio.mapr,
            50.hz(),
            clocks,
            &mut rcc.apb1
        );

        let sbus = {
            // Set up ppm output
            let mut gpioa = device.GPIOA.split(&mut rcc.apb2);
            let (tx_pin, rx_pin) = (
                gpioa.pa9.into_alternate_push_pull(&mut gpioa.crh),
                gpioa.pa10
            );

            let mut serial = serial::Serial::usart1(
                device.USART1,
                (tx_pin, rx_pin),
                &mut afio.mapr,
                serial::Config::default()
                    .baudrate(100_000.bps())
                    .parity_even()
                    .stopbits(serial::StopBits::STOP2),
                clocks,
                &mut rcc.apb2
            );
            serial.listen(serial::Event::Rxne);

            let (_tx, rx) = serial.split();
            rx
        };

        unsafe{BYTE_QUEUE = Some(Queue::new())};
        let (byte_tx, byte_rx) = unsafe{BYTE_QUEUE.as_mut().unwrap().split()};

        unsafe{SBUS_QUEUE = Some(Queue::new())};
        let (sbus_tx, sbus_rx) = unsafe{SBUS_QUEUE.as_mut().unwrap().split()};

        let sbus_decoder = SbusDecoder::new(byte_rx, sbus_tx);

        init::LateResources {
            SBUS: sbus,
            BYTE_TX: byte_tx,
            SBUS_RX: sbus_rx,
            SBUS_DECODER: sbus_decoder,
            PWM_CHANNELS: pwm_channels,
            LED: led
        }
    }

    #[idle(resources = [PWM_CHANNELS, SBUS_DECODER, SBUS_RX, LED])]
    fn idle() -> ! {
        let pwm = resources.PWM_CHANNELS;
        pwm.0.set_duty(
                pwm.0.get_max_duty() / 4
            );
        pwm.1.set_duty(
                pwm.1.get_max_duty() / 2
            );
        pwm.2.set_duty(
                3 * (pwm.2.get_max_duty() / 4)
            );
        pwm.3.set_duty(
                pwm.3.get_max_duty()
            );
        pwm.0.enable();
        pwm.1.enable();
        pwm.2.enable();
        pwm.3.enable();
        loop {
            resources.SBUS_DECODER.process().expect("Fatal decoding error");
            // hprintln!("{:?}", state);

            if let Some(message) = resources.SBUS_RX.dequeue() {
                match message {
                    Ok(message) => {
                        let fractional = message.channels[0] as f32 / 2048.;
                        pwm.0.set_duty(
                            (pwm.0.get_max_duty() as f32 * fractional) as u16
                        );
                        resources.LED.set_high();
                    }
                    Err(sbus::Error::Failsafe(message)) => {
                        resources.LED.set_low();
                    }
                    Err(e) => {
                        // TODO: Handle
                        // hprintln!("{:?}", e);
                    }
                }
            }
            asm::wfi();
        }
    }

    #[interrupt(resources = [SBUS, BYTE_TX])]
    fn USART1() {
        resources.BYTE_TX.enqueue(block!(resources.SBUS.read())).unwrap();
    }
};
